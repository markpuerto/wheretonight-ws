Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # resources :users, except: [:new, :edit]

  # resources :users, only: [:index, :create, :update, :show, :delete]

  resources :users, only: [:show, :index] do
    put "status"
  end

  # resources :events, only: [:index, :create, :show]

  namespace :api, path: '' do
    namespace :v1 do
      # scope 'admins' do
      resources :users, except: [:index, :new, :create, :edit, :update, :destroy, :show] do
        # collection {
          resources :events, only: [:index, :create, :show]
        # }
      end
      # end
    end

    namespace :v2 do
      resources :events, only: [:index, :create, :show]
    end
  end
end
