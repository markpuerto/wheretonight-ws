# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180720054754) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "actions", force: :cascade do |t|
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "place_id"
    t.integer "category", default: 0
    t.index ["place_id"], name: "index_actions_on_place_id"
    t.index ["user_id"], name: "index_actions_on_user_id"
  end

  create_table "audiences", force: :cascade do |t|
    t.bigint "user_id"
    t.integer "audienceable_id"
    t.string "audienceable_type"
    t.boolean "is_active", default: true
    t.boolean "is_disabled", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_audiences_on_user_id"
  end

  create_table "events", force: :cascade do |t|
    t.string "title"
    t.time "start_time_at"
    t.time "end_time_at"
    t.datetime "start_at"
    t.text "about"
    t.bigint "user_id"
    t.string "image"
    t.boolean "is_private", default: false
    t.boolean "is_visible", default: false
    t.integer "audience", default: 0
    t.boolean "is_active", default: true
    t.boolean "is_disabled", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "place_id"
    t.index ["place_id"], name: "index_events_on_place_id"
    t.index ["user_id"], name: "index_events_on_user_id"
  end

  create_table "friendships", force: :cascade do |t|
    t.bigint "user_id"
    t.integer "friend_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_friendships_on_user_id"
  end

  create_table "groups", force: :cascade do |t|
    t.bigint "user_id"
    t.string "title"
    t.text "about"
    t.boolean "is_private", default: false
    t.boolean "is_visible", default: false
    t.integer "audience", default: 0
    t.string "image"
    t.boolean "is_active", default: true
    t.boolean "is_disabled", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_groups_on_user_id"
  end

  create_table "images", force: :cascade do |t|
    t.string "imageable_type"
    t.bigint "imageable_id"
    t.boolean "is_active", default: true
    t.boolean "is_disabled", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "url"
    t.index ["imageable_type", "imageable_id"], name: "index_images_on_imageable_type_and_imageable_id"
  end

  create_table "places", force: :cascade do |t|
    t.string "name"
    t.float "latitude", default: 0.0
    t.float "longitude", default: 0.0
    t.string "address"
    t.boolean "is_active", default: true
    t.boolean "is_disabled", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "reports", force: :cascade do |t|
    t.bigint "user_id"
    t.integer "blocked_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_reports_on_user_id"
  end

  create_table "universities", force: :cascade do |t|
    t.string "name"
    t.boolean "is_active", default: true
    t.boolean "is_disabled", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "university_id"
    t.string "username"
    t.string "image"
    t.string "fb_url"
    t.string "twitter_url"
    t.string "instagram_url"
    t.string "google_url"
    t.integer "gender", default: 0
    t.boolean "is_student", default: true
    t.boolean "is_private", default: true
    t.text "about"
    t.boolean "is_show_name", default: true
    t.boolean "is_show_plans", default: true
    t.boolean "is_show_checkins", default: true
    t.boolean "is_alert_message", default: true
    t.boolean "is_alert_notification", default: true
    t.boolean "is_alert_event", default: true
    t.string "name"
    t.string "token"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["university_id"], name: "index_users_on_university_id"
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true
  end

  add_foreign_key "events", "places"
end
