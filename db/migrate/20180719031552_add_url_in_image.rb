class AddUrlInImage < ActiveRecord::Migration[5.1]
  def up
    add_column :images, :url, :string
  end

  def down
    remove_column :images, :url
  end
end
