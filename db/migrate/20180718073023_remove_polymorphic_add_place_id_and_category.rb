class RemovePolymorphicAddPlaceIdAndCategory < ActiveRecord::Migration[5.1]
  def up
    change_table :actions do |t|
      t.remove_references :actionable, polymorphic: true
      t.belongs_to :place
      t.integer :category, default: 0
    end
  end

  def down
    change_table :actions do |t|
      t.references :actionable, polymorphic: true
    end
  end
end
