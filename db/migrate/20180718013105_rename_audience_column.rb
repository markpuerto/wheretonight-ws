class RenameAudienceColumn < ActiveRecord::Migration[5.1]
  def change
    rename_column :audiences, :audience_id, :audienceable_id
    rename_column :audiences, :audience_type, :audienceable_type
  end
end
