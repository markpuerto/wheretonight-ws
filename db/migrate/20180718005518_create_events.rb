class CreateEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :events do |t|
      t.string :title
      t.time :start_time_at
      t.time :end_time_at
      t.datetime :start_at
      t.text :about
      t.belongs_to :user
      t.string :image
      t.boolean :is_private, default: false
      t.boolean :is_visible, default: false
      t.integer :audience, default: 0
      t.boolean :is_active, default: true
      t.boolean :is_disabled, default: false
      t.timestamps
    end
  end
end
