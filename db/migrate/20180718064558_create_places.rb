class CreatePlaces < ActiveRecord::Migration[5.1]
  def change
    create_table :places do |t|
      t.string :name
      t.float :latitude, default: 0.0
      t.float :longitude, default: 0.0
      t.string :address
      t.boolean :is_active, default: true
      t.boolean :is_disabled, default: false
      t.timestamps
    end
  end
end
