class CreateGroups < ActiveRecord::Migration[5.1]
  def change
    create_table :groups do |t|
      t.belongs_to :user
      t.string :title
      t.text :about
      t.boolean :is_private, default: false
      t.boolean :is_visible, default: false
      t.integer :audience, default: 0
      t.string :image
      t.boolean :is_active, default: true
      t.boolean :is_disabled, default: false
      t.timestamps
    end
  end
end
