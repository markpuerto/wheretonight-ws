class CreateReports < ActiveRecord::Migration[5.1]
  def change
    create_table :reports do |t|
      t.belongs_to :user
      t.integer :blocked_id
      t.timestamps
    end
  end
end
