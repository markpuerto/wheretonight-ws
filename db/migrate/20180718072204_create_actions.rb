class CreateActions < ActiveRecord::Migration[5.1]
  def change
    create_table :actions do |t|
      t.belongs_to :user
      t.references :actionable, polymorphic: true, index: true
      t.timestamps
    end
  end
end
