class AddColumnsInUser < ActiveRecord::Migration[5.1]
  def up
    add_reference :users, :university, index: true
    add_column :users, :username, :string
    add_column :users, :image, :string
    add_column :users, :fb_url, :string
    add_column :users, :twitter_url, :string
    add_column :users, :instagram_url, :string
    add_column :users, :google_url, :string
    add_column :users, :gender, :integer, default: 0
    add_column :users, :is_student, :boolean, default: true
    add_column :users, :is_private, :boolean, default: true
    add_column :users, :about, :text
    add_column :users, :is_show_name, :boolean, default: true
    add_column :users, :is_show_plans, :boolean, default: true
    add_column :users, :is_show_checkins, :boolean, default: true
    add_column :users, :is_alert_message, :boolean, default: true
    add_column :users, :is_alert_notification, :boolean, default: true
    add_column :users, :is_alert_event, :boolean, default: true
  end

  def down
    remove_reference :users, :university, index: true
    remove_column :users, :username
    remove_column :users, :image
    remove_column :users, :fb_url
    remove_column :users, :twitter_url
    remove_column :users, :instagram_url
    remove_column :users, :google_url
    remove_column :users, :gender
    remove_column :users, :is_student
    remove_column :users, :is_private
    remove_column :users, :about
    remove_column :users, :is_show_name
    remove_column :users, :is_show_plans
    remove_column :users, :is_show_checkins
    remove_column :users, :is_alert_message
    remove_column :users, :is_alert_notification
    remove_column :users, :is_alert_event
  end
end
