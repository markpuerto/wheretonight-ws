class AddPlaceIdInPlace < ActiveRecord::Migration[5.1]
  def up
    add_reference :events, :place, foreign_key: true
  end

  def down
    remove_reference :events, :place
  end
end
