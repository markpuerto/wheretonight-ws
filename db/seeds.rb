universities = [
  { name: "University of Cebu - Banilad" }, { name: "University of Cebu - Main"},
  { name: "University of San Carlos"}, { name: "Cebu Normal University"},
  { name: "Cebu Institute of Technology - University"}, { name: "Cebu Technological University"}]

universities.each do |param|
  unless University.find_by(name: param[:name]).present?
    University.create(param)
  end
end

events = [
  { title: "Party in the Streets", start_time_at: Time.now, end_time_at: Time.now + 8.hour, start_at: DateTime.now, about: "This is for the people who love street dancing.", user_id: 1, place_id: 1 },
  { title: "Acquaintance Party", start_time_at: Time.now, end_time_at: Time.now + 8.hour, start_at: DateTime.now + 3.days, about: "All college level", user_id: 2, is_private: true, is_visible: true, audience: 0, place_id: 2 },
  { title: "Drinks on the Beach", start_time_at: Time.now, end_time_at: Time.now + 8.hour, start_at: DateTime.now + 2.days, about: "Drinking party in the beach.", user_id: 1, place_id: 3 }]

events.each do |param|
  event = Event.find_by(title: param[:title])

  if event.present?
    event.update(param)
  else
    event = Event.create(param)
    Audience.create(user_id: param[:user_id], audienceable_id: event.id, audienceable_type: "Event")
  end
end

groups = [
  { title: "Nerd People", about: "For nerd people", is_private: true, is_visible: true, user_id: 2 },
  { title: "I love Cebu", about: "Tourist here in Cebu", user_id: 1 },
  { title: "Sex or Chocolates", about: "What will you choose", user_id: 2 }
]

groups.each do |param|
  group = Group.find_by(title: param[:title])

  if group.present?
    group.update(param)
  else
    group = Group.create(param)
    Audience.create(user_id: param[:user_id], audienceable_id: group.id, audienceable_type: "Group")
  end
end

places = [
  { name: "JCentre Mall", address: "A.S. Fortuna St. Mandaue City" },
  { name: "Benedicto College", address: "A.S. Fortuna St. Mandaue City" },
  { name: "FPN Cebu", address: "A.S. Fortuna St. Mandaue City" }
]

places.each do |param|
  place = Place.find_by(name: param[:name])
  if place.present?
    place.update(param)
  else
    Place.create(param)
  end
end
