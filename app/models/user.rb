class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  validates :username, presence: true, uniqueness: true

  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable

  belongs_to :university

  has_many :events_organized, foreign_key: :user_id, class_name: "Event"
  has_many :groups_organized, foreign_key: :user_id, class_name: "Group"

  has_many :audiences, -> { where("audiences.is_active = true and audiences.is_disabled = false") }
  has_many :events, -> { where("events.is_disabled = false") }, through: :audiences, source: :audienceable, source_type: "Event"
  has_many :groups, -> { where("groups.is_disabled = false") }, through: :audiences, source: :audienceable, source_type: "Group"

  has_many :friendships
  has_many :friends, through: :friendships

  has_many :inverse_friendships, class_name: "Friendship", foreign_key: :friend_id
  has_many :inverse_friends, through: :inverse_friendships, source: :user

  has_many :actions
  has_many :likes, -> { where(actions: { category: 0 }) }, through: :actions, source: :place
  has_many :check_ins, -> { where(actions: { category: 1 }) }, through: :actions, source: :place

  has_many :images, as: :imageable

  has_many :reports
  has_many :blockeds, through: :reports

  scope :filtered, -> (is_student, is_private) {
    where("is_student = ? and is_private = ?", is_student, is_private)
  }

  before_create :normalize_name
  after_create :generate_session_token

  def first_name
    self.name.split(" ").first
  end

  def last_name
    self.name.split(" ").pop
  end

  private
    def normalize_name
      self.name = name.titleize
    end

    def generate_session_token
      secret = Digest::SHA1.hexdigest(SecureRandom.urlsafe_base64)
      self.token = User.find_by(token: secret).present? ? generate_session_token : secret
      self.save
    end
end
