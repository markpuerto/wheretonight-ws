class Event < ApplicationRecord
  belongs_to :user
  belongs_to :place

  has_many :audiences, as: :audienceable
  has_many :images, as: :imageable

  # has_many :attendees, through: :audiences

  accepts_nested_attributes_for :place

  # default_scope { where(is_disabled: false) }

  scope :plans, -> {
    select{|event| event.start_at > DateTime.now }
  }

  scope :activated, -> {
    select{ |event| event.is_active == true && event.is_disabled == false }.sort_by{|event| event.id }
  }

  scope :filtered, -> (is_active, is_disabled){
    select{ |event| event.is_active == true && event.is_disabled == false }.sort_by{|event| event.id }
  }
end
