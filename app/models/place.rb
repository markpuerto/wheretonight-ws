class Place < ApplicationRecord
  has_many :events
  has_many :actions

  has_many :actions
  has_many :likes, -> { where(actions: { category: 0 }) }, through: :actions, source: :user
  has_many :check_ins, -> { where(actions: { category: 1 }) }, through: :actions, source: :user
end
