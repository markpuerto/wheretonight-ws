class Audience < ApplicationRecord
  belongs_to :user
  belongs_to :audienceable, polymorphic: true
end
