class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception, if: Proc.new { |c| c.request.format == 'application/json' }

  def sign_in_authorization
    @user = User.find_by(token: params[:token])

    if @user.present? && params[:token].present?
      return @user
    else
      return render json: { error: "Unauthorized access on the API", status: 300 }, status: 200
    end
  end
end
