class Api::V2::EventsController < ApplicationController
  before_action :sign_in_authorization

  include ApplicationHelper

  def index
    events = Event.includes(:user, :place, :audiences).activated.sort_by{|event| event.created_at }.reverse!
    total_events = events.length

    events = events.drop(params[:off_set].to_i).first(10) # this is for eager loading

    @events = []
    events.each do |event|
      e = event_params(event, @user.id).merge!(audiences: event.audiences)
      @events.push(e)
    end

    off_set = params[:off_set].to_i + 10
    @pagination = {
      events: total_events,
      off_set: off_set,
      load_more: off_set < total_events ? 1 : 0,
      event_url: api_v2_events_path(user_id: params[:user_id], off_set: off_set)
    }

    render json: { events: @events, pagination: @pagination, status: 200 }, status: 200
  end

  def create
    event = Event.create(params_event)
    if event.save
      Audience.create(user_id: event.user_id, audienceable_id: event.id, audienceable_type: "Event")
      render json: { event: event_params(event, params[:user_id]), status: 200 }, status: 200
    else
      render json: { status: 300 }, status: 200
    end
  end

  def show
    event = Event.includes(:user, :place).find_by(id: params[:id])

    @event = event_params(event, params[:user_id].to_i)
    render json: { event: @event, status: 200 }, status: 200
  end

  private
    def params_event
      params.require(:event).permit(:title, :start_time_at, :end_time_at, :start_at, :about, :user_id, :place_id, :is_private, :audience)
    end
end
