class UsersController < ApplicationController

  def create

  end

  def show
    user = User.includes(:events, :groups, :events_organized).find_by(id: params[:id])

    @user = { user: { name: user.name, email: user.email, events: user.events, groups: user.groups, organized: user.events_organized } }
    # respond_to do |format|
    #   format.html
    #   format.json { render json: { user: @user } }
    # end

    render json: @user, status: 200
  end

  def update

  end

  def index
    users = User.includes(:events, :groups, :events_organized, :groups_organized).order("created_at asc")
    # users = User.order("created_at asc")

    @users = []

    users.each do |user|
      @users.push({ user: { name: user.name, email: user.email, events: user.events, groups: user.groups, events_organized: user.events_organized, groups_organized: user.groups_organized }})
    end

    render json: { users: @users }
  end

  def destroy

  end

  def new

  end

  def edit

  end

  def status

  end

end
