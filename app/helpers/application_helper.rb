module ApplicationHelper

  def event_params(event, user_id)
    @event = {
      id: event.id,
      title: event.title,
      about: event.about,
      time: event.start_time_at.strftime("%I:%M %p").to_s + " - " + event.end_time_at.strftime("%I:%M %p"),
      date: event.start_at.strftime("%B %d, %Y"),
      image: event.image,
      is_private: convert_bool(event.is_private),
      is_organizer: event.user_id == user_id ? 1 : 0,
      rsvp: 0,
      organizer: user_params(event.user),
      place: place_params(event.place, false)
    }
  end

  def place_params(place, has_geolocation)
    @place = {
      id: place.id,
      name: place.name,
      address: place.address
    }

    if has_geolocation == true
      @place.merge!(latitude: place.latitude, longitude: place.longitude)
    end

    return @place
  end

  def group_params(group)

  end

  def user_params(user)
    user = {
      id: user.id,
      name: user.username,
      image: user.image
    }
  end

  def convert_bool(param)
    param == true ? 1 : 0
  end
end
